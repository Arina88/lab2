﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Название экзамена
            string name = "Программирование";
            //Дата и время начала экзамена
            DateTime date1 = new DateTime(2021, 9, 15);
            DateTime date2 = new DateTime(2021, 9, 15, 10, 00, 00);
            //Время окончания экзамена
            DateTime date3 = new DateTime(2021, 9, 15, 13, 30, 00);
            // Количество заданий
            int a;
            a = 10;
            // Количество баллов за каждое задание
            int b;
            b = 3;
            //Максимальное количество баллов за весь экзамен
            int s;
            s = a * b;
            //Номер аудитории
            int w;
            w = 325;
            //Количество сдающих
            int t;
            t = 45;
            //Вероятность успешного написания экзамена
            double d;
            d = s * 0.5/ t * 100;
            //Среднее время выполнения работы учеником 
            double m;
            m=s*t*7/60;
            //Коэффициент правильности прорграммы
            double ko;
            ko = (s * 55 + 33 * t*a - m * 2 )/( d * 1100);


            //Необходимые условия для успешного выполнения задания
            //Наличие таких программ,как
            string name1 = "Visual Studio 2022";
            string name2 = "Gitlab";
            string name3 = "любая программа для демонстрации экрана";
            Console.WriteLine($"Экзамен по предмету:{name}");
            Console.WriteLine($"Дата проведения экзамена:{date1.ToLongDateString()}");
            Console.WriteLine($"Время начала:{date2.ToShortTimeString()}");
            Console.WriteLine($"Время окончания:{date3.ToShortTimeString()}");
            Console.WriteLine($"Количество заданий:{a}");
            Console.WriteLine($"Максимальная оценка за тест:{s} баллов");
            Console.WriteLine($"Количество сдающих:{t}человек");
            Console.WriteLine($"Номер аудитории:{w}");
            Console.WriteLine($"Необходимо наличие таких программ,как:{name1},{name2},{name3};");
            Console.WriteLine($"Вероятность успешной сдачи экзамена: {d}%");
            Console.WriteLine($"Среднее время выполнения работы каждым учеником:{m} минут");
            Console.WriteLine($"Коэффициент правильности программы:{ko}");
           // Console.WriteLine($)
            Console.ReadKey();

        }
    }
}
